import os
import pyinotify
import subprocess
import sys
import time
import argparse

class EventHandler(pyinotify.ProcessEvent):

    def __init__(self):
        self.channels = {}

        self.valid_commands = {
                               'clear':self.clear,
                               'clear_all':self.clear_all,
                              }

    def clear(self, *channels):
        for ch in channels:
            try:
                self.channels.pop(ch)
                self.update_pipe()
            except KeyError:
                print(ch+" is not a tracked channel.\n")

    def clear_all(self):
        self.channels = {}
        self.update_pipe()

    def update_pipe(self):
        topipe = " ".join([ channel+"["+str(unread)+"]" for channel, unread
                            in self.channels.iteritems() ])
        temp = sys.stdout
        sys.stdout = open(ARGS.fifo, 'w')
        print(topipe)
        sys.stdout.close()
        sys.stdout = temp


    def process_IN_MODIFY(self, event):
        print(str(event))

        channelname = event.name.strip('.log')

        if channelname == 'commandpipe.ascii':
            line = subprocess.check_output(['tail', '-1', event.pathname])

            line_clean = line.strip().split()
            command = line_clean[0]
            command_args = line_clean[1:]

            if command in self.valid_commands:
                msg = "\n Received command: "+command+" "+" ".join(command_args)+"\n"
                print(msg)
                slowflush(msg)
                self.valid_commands[command](*command_args)
            else:
                print("\n Received invalid command: "+command+" ".join(command_args)+"\n")

        elif channelname not in self.channels:
             self.channels.update({channelname:1})
             self.update_pipe()
        else:
             self.channels[channelname] += 1
             self.update_pipe()


def slowflush(message, delay=0.02):
    temp = sys.stdout
    sys.stdout = open(ARGS.feed, 'w')

    for i in range(len(message)):
        print(message[:i])
        sys.stdout.flush()
        time.sleep(delay)

    sys.stdout.close()
    sys.stdout = temp

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--watchdir', '-w', required=True)
    parser.add_argument('--delay', '-d', default=0.02, type=float)
    parser.add_argument('--fifo', '-f',
                        default='/home/bernie/pijp')
    parser.add_argument('--feed', '-F',
                        default='/home/bernie/pipereader')
    return parser.parse_args()

if __name__ == "__main__":
    ARGS = get_args()

    # Instanciate a new WatchManager (will be used to store watches).
    wm = pyinotify.WatchManager()
    mask = pyinotify.IN_MODIFY | pyinotify.IN_CREATE | pyinotify.IN_DELETE

    # Associate this WatchManager with a Notifier (will be used to report and
    # process events).
    handler = EventHandler()
    notifier = pyinotify.Notifier(wm, handler)

    # Add a new watch on /tmp for ALL_EVENTS.
    #wm.add_watch('../Freenode', pyinotify.ALL_EVENTS)
    wm.add_watch(ARGS.watchdir, mask, rec=True)

    # Loop forever and handle events.
    notifier.loop()
